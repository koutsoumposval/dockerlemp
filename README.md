# Simple docker LEMP stack #

### Stack ###

**Nginx**
* Image: tutum/nginx
* Port: 80:80

**MySQL**
* Image: mariadb
* Port: 3306:3306
* Password: root

**PHP**
* Image: php:fpm
* Port: 9000:9000

**Phpmyadmin**
* Image: phpmyadmin/phpmyadmin
* Port: 8181:80
* Username: root
* Password: root

### Installation ###

**Required**
* docker-composer
* docker-machine

**Installation Steps**
1. Obtain docker-machine id
2. Update ./nginx/default servername
3. docker-composer run

### Contact ###

Koutsoumpos Chrysovalantis <info@newweb.gr>